﻿using System;
using System.Collections.Generic;

namespace ProfBoard.DataAccess.Models
{
    public partial class IntValue
    {
        public DateTime AgentDateTime { get; set; }
        public long Id { get; set; }
        public long VaribleId { get; set; }
        public short? Value { get; set; }
        public bool IsCondition { get; set; }
        public DateTime InsertDateTime { get; set; }
        public short SourceId { get; set; }
    }
}
