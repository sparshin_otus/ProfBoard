﻿using System;
using System.Collections.Generic;

namespace ProfBoard.DataAccess.Models
{
    public partial class PlcVaribleSetting
    {
        public long Id { get; set; }
        public long TableSettingId { get; set; }
        public string Name { get; set; } = null!;
        public int Db { get; set; }
        public int StartByteAdr { get; set; }
        public int StartBitAdr { get; set; }
        public int VarTypeCode { get; set; }
        public int DataTypeCode { get; set; }
        public string Info { get; set; } = null!;
        public bool Dbserializeble { get; set; }
        public bool OnlyChanged { get; set; }
        public string? SourceFilter { get; set; }
    }
}
