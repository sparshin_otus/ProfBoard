﻿using System;
using System.Collections.Generic;

namespace ProfBoard.DataAccess.Models
{
    public partial class PlcCpuType
    {
        public long Id { get; set; }
        public int Code { get; set; }
        public string? Description { get; set; }
    }
}
